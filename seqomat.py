#!/usr/bin/python
import time
import random
import json
import inotifyx
import midi     # from https://github.com/vishnubob/python-midi/
import midi.sequencer as sequencer

class blinkomatstate:
    def __init__(self):
        # states
        self.notes=          [ -1,       1,        0,      2 ]       # notes (off, blue, green, red)
        self.lengths=        [ (1,5), (1,5), (1,5), (1,5) ]  # length (min,max) in seconds
        self.probabilities=  [ 4,        4,        4,      4 ]       # probability for state switching
        self.maxprob=        [ ]    # filled by initprobtbl()

        self.curstate= 0   # current state
        self.lastswitch= 0 # time of last state switch in seconds
        self.nextswitch= 0 # time of next switch in seconds

        self.seq= None       # a sequencer.sequencerWrite() connected to Blinkomat3000
        
        self.time_rand_oversmpl= 1   # the higher this integer, the more the state length will tend to the mean of min+max
        
        self.configpath= "config.json"
        self.watchfd= None
        self.configwatch= None

        # open midi port
        self.seq= sequencer.SequencerWrite()
        client, port = sequencer.SequencerHardware().get_client_and_port("Blinkomat3000", "Blinkomat3000 MIDI 1")
        self.seq.subscribe_port(client, port)
        
        # create inotify watch for config
        self.watchfd= inotifyx.init()
        self.configwatch= inotifyx.add_watch(self.watchfd, self.configpath, inotifyx.IN_CLOSE_WRITE)
        self.reloadconfig()
        
        self.initprobtbl()

    # initialize probability table
    def initprobtbl(self):
        maxprob= 0
        for p in self.probabilities:
            maxprob+= p
            self.maxprob.append(maxprob)
    
    # reload config.json
    def reloadconfig(self):
        print("RELOADCONFIG")
        try:
            with open(self.configpath) as f:
                j= json.loads(f.read())
                
                # do some basic sanity checks
                assert len(j['state_length'])==len(j['state_probability'])==4, "state_length and state_probability must be arrays with length 4"
                for i in j['state_length']:
                    assert len(i)==2, "state_length entries must be arrays (min,max length in seconds)"
                
                self.lengths= j['state_length']
                self.probabilities= j['state_probability']
                self.initprobtbl()
                self.nextswitch= 0
                
        except Exception as ex: # something went wrong, possibly syntax error in json
            print "Loading config failed: %s" % str(ex)
    
    # reload config.json whenever it changed
    def checkconfig(self):
        events= inotifyx.get_events(self.watchfd, 0)
        if len(events)==0: return
        self.reloadconfig()

    # switch to given state
    def switchto(self, state):
        print("STATE %d" % state)
        t= time.time()
        self.curstate= state
        self.lastswitch= t
        tmin= self.lengths[state][0]
        tmax= self.lengths[state][1]
        randsum= 0
        for i in range(self.time_rand_oversmpl):
            randsum+= random.uniform(tmin, tmax)
        self.nextswitch= t + randsum/self.time_rand_oversmpl
        print("nextswitch: %.2f" % (self.nextswitch-self.lastswitch))

    # switch to a new random state
    def randswitch(self):
        rnd= random.random()*self.maxprob[-1]
        for state in range(len(self.maxprob)):
            maxprob= self.maxprob[state]
            if rnd < maxprob:
                self.switchto(state)
                break

    # blink with current state color
    def blink(self):
        if self.curstate<=0: return  # led off
        ev= midi.events.NoteOnEvent()
        ev.pitch= self.notes[self.curstate]
        ev.velocity= 127
        self.seq.event_write(ev, direct= True)
    
    # called periodically
    def tick(self):
        t= time.time()
        if t>self.nextswitch:
            self.randswitch()
        self.blink()
        self.checkconfig()
        time.sleep(.125)


if __name__ == '__main__':
    blinker= blinkomatstate()
    while True:
        blinker.tick()